# ORGANUSER

Programa escrito em **Bash** para manipulação de usuários *Unix e Samba*.

A intenção é utilizá-lo para facilitar operações com usuários e grupos em servidores de arquivos usando o **SAMBA**.

Com ele pode-se criar e excluir usuários e grupos, adicionar e remover usuários de grupos e alterar senhas dos usuários.

Ele trabalha com ambas as contas de usuário ao mesmo tempo, Unix e Samba. Assim, ao criar, excluir ou alterar a senha de um usuário, o programa o faz para as duas contas.

O esquema de gerenciamento de contas por trás do programa é o mais **simples** possível, usandos as opções mais básicas de comandos como o *useradd*, o *userdel*, o *gpasswd*, o *passwd*, o *smbpasswd* e a análise dos arquivos contendo os dados de grupos e usuários */etc/passwd* e */etc/groups*, através **expressões regulares**.

O ORGANUSER foi escrito pensando numa utilização para 999 usuários e 999 grupos, mas nada que uma alteração nas *expressões regulares* não resolva.

# screenshot
![organuser](20200902-000632.avi.gif)
